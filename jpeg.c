#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <assert.h>
#include <fcntl.h>
#include <jpeglib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/types.h>
#include <termio.h>
#include <unistd.h>

#define BUFFERSIZE 65536

static void
usage()
{
    fprintf(stderr, "usage: jpeg [ -w width ] [ -h height ] <jpeg image>\n");
    exit(1);
}

int
main(int argc, char *argv[])
{
    int lines = 24, columns = 80;
    struct winsize win;
    if (ioctl(0, TIOCGWINSZ, &win) == 0) {
        lines = win.ws_row;
        columns = win.ws_col;
    } else {
        const char *p = getenv("LINES");
        if (p)
            lines = atoi(p);
        p = getenv("COLUMNS");
        if (p)
            columns = atoi(p);
    }
    int c;
    while ((c = getopt(argc, argv, "w:h:")) != -1) {
        switch (c) {
            case 'w': columns = atoi(optarg); break;
            case 'h': lines = atoi(optarg); break;
            default: usage();
        }
    }
    struct jpeg_decompress_struct decomp;
    struct jpeg_error_mgr error;
    decomp.err = jpeg_std_error(&error);
    jpeg_create_decompress(&decomp);
    if (argc == optind)
        usage();
    FILE *f = fopen(argv[optind], "r");
    if (f == 0)
        exit(-1);
    jpeg_stdio_src(&decomp, f);
    jpeg_read_header(&decomp, 1);
    jpeg_start_decompress(&decomp);
    int samplex = decomp.output_width / columns; // number of image pixels across per char cell.
    int sampley = decomp.output_height / lines; // number of image pixels down per char cell.

    // Buffer big enough for one scanline.
    JSAMPARRAY buffer = decomp.mem->alloc_sarray((j_common_ptr)&decomp, JPOOL_IMAGE,
            decomp.output_width * decomp.output_components, 1);
    unsigned char *linedata = buffer[0];
    int *intensity = calloc(columns, sizeof (int));
    int *samples = calloc(columns, sizeof (int));
    int linesDone = 0;
    while (decomp.output_scanline < decomp.output_height) {
        int havelines = jpeg_read_scanlines(&decomp, buffer, 1);
        if (havelines == 0)
            break;
        int to = 0;
        for (int col = 0; col < decomp.output_width; ++col) {
            int off = col * decomp.output_components;
            for (int i = 0; i < decomp.output_components; ++i) {
                intensity[to] += linedata[off + i]; // XXX: bAlance these RGB-wise.
                samples[to]++;
            }
            if ((col + 1) % samplex == 0) {
                if (++to >= columns)
                    break;
            }
        }

        if (++linesDone % sampley == 0) {
            for (int i = 0; i < columns; ++i) {
                const char bright[] = " .:;o\%OQB#";
                intensity[i] *= sizeof bright - 1;
                intensity[i] /= 256 * samples[i];
                putc(bright[intensity[i]], stdout);
                intensity[i] = 0;
                samples[i] = 0;
            }
            putc('\n', stdout);
        }
    }
    free(intensity);
    free(samples);
    jpeg_finish_decompress(&decomp);
    jpeg_destroy_decompress(&decomp);
    return 0;
}
