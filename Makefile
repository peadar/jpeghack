CFLAGS += -std=c99 -D_XOPEN_SOURCE=500 -g -I /usr/local/include

jpeg: jpeg.o
	$(CC) -L/usr/local/lib -Wl,-rpath /usr/local/lib -o $@ jpeg.o -ljpeg
